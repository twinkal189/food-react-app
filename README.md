This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Application Functionality

Food application with React and Redux with ant design.
- Dashboard has list of food item with respect to categories.
- On selection of header category navigate to particular section 
- User can add the food item / combination by clicking on food item. 
- There is popup available for detail view and create the custom combination for same.
- User can add as many item he wants. There is the basket facility provided for the view the current cart items.
- https://foodappreact.netlify.app/

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

### Application Snapshot
![Application snapshot](screenshots/main.png)
![Application snapshot](screenshots/add-item-popup.png)
![Application snapshot](screenshots/choose-second-item.png)
![Application snapshot](screenshots/add-extra-item-popup.png)
![Application snapshot](screenshots/basket.png)
